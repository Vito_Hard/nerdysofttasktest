package com.nerdysoft.test.service;

import java.util.List;

import com.nerdysoft.test.entity.LegalRooms;

public interface LegalRoomsService {

	void saveLegalRooms(LegalRooms legalRooms);

	LegalRooms findLegalRoomsById(Long id);

	void deleteLegalRoomsById(Long id);

	List<LegalRooms> findAllRooms();
}
