package com.nerdysoft.test.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nerdysoft.test.entity.LegalRooms;
import com.nerdysoft.test.repository.LegalRoomsRepository;
import com.nerdysoft.test.service.LegalRoomsService;

@Service
public class LegalRoomsServiceImpl implements LegalRoomsService {

	private LegalRoomsRepository legalRoomsRepository;

	@Autowired
	public LegalRoomsServiceImpl(LegalRoomsRepository legalRoomsRepository) {
		this.legalRoomsRepository = legalRoomsRepository;
	}

	@Override
	public void saveLegalRooms(LegalRooms legalRooms) {
		legalRoomsRepository.save(legalRooms);
	}

	@Override
	public LegalRooms findLegalRoomsById(Long id) {

		return legalRoomsRepository.findUserById(id);
	}

	@Override
	@Transactional
	public void deleteLegalRoomsById(Long id) {
		// legalRoomsRepository.deleteLegalRoomsById(id);
		legalRoomsRepository.deleteById(id);
	}

	@Override
	public List<LegalRooms> findAllRooms() {

		return legalRoomsRepository.findAll();
	}

}
