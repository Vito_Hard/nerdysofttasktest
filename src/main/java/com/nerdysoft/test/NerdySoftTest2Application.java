package com.nerdysoft.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NerdySoftTest2Application {

	public static void main(String[] args) {
		SpringApplication.run(NerdySoftTest2Application.class, args);
	}

}
