package com.nerdysoft.test.basecontroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.nerdysoft.test.entity.LegalRooms;
import com.nerdysoft.test.service.LegalRoomsService;

@Controller
public class BaseController {

	@Autowired
	private LegalRoomsService legalRoomsService;

	@GetMapping("/")
	public String home() {

		return "home";
	}

	@GetMapping("/list")
	public String list(Model model) {

		model.addAttribute("all", legalRoomsService.findAllRooms());

		return "list";
	}

	@GetMapping("/list/delete/{id}")
	public String delete(@PathVariable("id") String id, Model model) {
		legalRoomsService.deleteLegalRoomsById(Long.parseLong(id));
		return "redirect:/list";
	}

	@GetMapping("/list/modificationData/{id}")
	public String update(@PathVariable("id") String id, Model model) {
		model.addAttribute("data", legalRoomsService.findLegalRoomsById(Long.parseLong(id)));
		return "update";
	}

	@PostMapping("/update")
	public String updateData(@ModelAttribute("update") LegalRooms legalRooms) {

		legalRoomsService.saveLegalRooms(legalRooms);

		return "redirect:/list/modificationData/" + legalRooms.getId();
	}

}
