package com.nerdysoft.test.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.nerdysoft.test.entity.LegalRooms;

@Repository
public interface LegalRoomsRepository extends JpaRepository<LegalRooms, Long> {

	@Query("SELECT l FROM LegalRooms l WHERE l.id = :id")
	LegalRooms findUserById(@Param("id") Long id);

}
