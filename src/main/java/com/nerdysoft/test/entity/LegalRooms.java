package com.nerdysoft.test.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "legal_rooms")
public class LegalRooms {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "first_room_param_x")
	@Min(value = 0, message = "parameter >= 0")
	@Max(value = 35, message = "parameter <= 35")
	@NotNull(message = "Not Null")
	private String firstRoomParamX;

	@Column(name = "first_room_param_y")
	@Min(value = 0, message = "parameter >= 0")
	@Max(value = 35, message = "parameter <= 35")
	@NotNull(message = "Not Null")
	private String firstRoomParamY;

//////////////////////////////////////////////

	@Column(name = "second_room_param_x")
	@Min(value = 0, message = "parameter >= 0")
	@Max(value = 35, message = "parameter <= 35")
	@NotNull(message = "Not Null")
	private String secondRoomParamX;

	@Column(name = "second_room_param_y")
	@Min(value = 0, message = "parameter >= 0")
	@Max(value = 35, message = "parameter <= 35")
	@NotNull(message = "Not Null")
	private String secondRoomParamY;

	//////////////////////////////////

	@Column(name = "third_room_param_x")
	@Min(value = 0, message = "parameter >= 0")
	@Max(value = 35, message = "parameter <= 35")
	@NotNull(message = "Not Null")
	private String thirdRoomParamX;

	@Column(name = "third_room_param_y")
	@Min(value = 0, message = "parameter >= 0")
	@Max(value = 35, message = "parameter <= 35")
	@NotNull(message = "Not Null")
	private String thirdRoomParamY;

	///////////////////////////////////////

	@Column(name = "fourth_room_param_x")
	@Min(value = 0, message = "parameter >= 0")
	@Max(value = 35, message = "parameter <= 35")
	@NotNull(message = "Not Null")
	private String fourthRoomParamX;

	@Column(name = "fourth_room_param_y")
	@Min(value = 0, message = "parameter >= 0")
	@Max(value = 35, message = "parameter <= 35")
	@NotNull(message = "Not Null")
	private String fourthRoomParamY;

	public LegalRooms(String firstRoomParamX, String firstRoomParamY, String secondRoomParamX, String secondRoomParamY,
			String thirdRoomParamX, String thirdRoomParamY, String fourthRoomParamX, String fourthRoomParamY) {
		this.firstRoomParamX = firstRoomParamX;
		this.firstRoomParamY = firstRoomParamY;
		this.secondRoomParamX = secondRoomParamX;
		this.secondRoomParamY = secondRoomParamY;
		this.thirdRoomParamX = thirdRoomParamX;
		this.thirdRoomParamY = thirdRoomParamY;
		this.fourthRoomParamX = fourthRoomParamX;
		this.fourthRoomParamY = fourthRoomParamY;
	}

	public LegalRooms(Long id, String firstRoomParamX, String firstRoomParamY, String secondRoomParamX,
			String secondRoomParamY, String thirdRoomParamX, String thirdRoomParamY, String fourthRoomParamX,
			String fourthRoomParamY) {
		this.id = id;
		this.firstRoomParamX = firstRoomParamX;
		this.firstRoomParamY = firstRoomParamY;
		this.secondRoomParamX = secondRoomParamX;
		this.secondRoomParamY = secondRoomParamY;
		this.thirdRoomParamX = thirdRoomParamX;
		this.thirdRoomParamY = thirdRoomParamY;
		this.fourthRoomParamX = fourthRoomParamX;
		this.fourthRoomParamY = fourthRoomParamY;
	}

	public LegalRooms() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstRoomParamX() {
		return firstRoomParamX;
	}

	public void setFirstRoomParamX(String firstRoomParamX) {
		this.firstRoomParamX = firstRoomParamX;
	}

	public String getFirstRoomParamY() {
		return firstRoomParamY;
	}

	public void setFirstRoomParamY(String firstRoomParamY) {
		this.firstRoomParamY = firstRoomParamY;
	}

	public String getSecondRoomParamX() {
		return secondRoomParamX;
	}

	public void setSecondRoomParamX(String secondRoomParamX) {
		this.secondRoomParamX = secondRoomParamX;
	}

	public String getSecondRoomParamY() {
		return secondRoomParamY;
	}

	public void setSecondRoomParamY(String secondRoomParamY) {
		this.secondRoomParamY = secondRoomParamY;
	}

	public String getThirdRoomParamX() {
		return thirdRoomParamX;
	}

	public void setThirdRoomParamX(String thirdRoomParamX) {
		this.thirdRoomParamX = thirdRoomParamX;
	}

	public String getThirdRoomParamY() {
		return thirdRoomParamY;
	}

	public void setThirdRoomParamY(String thirdRoomParamY) {
		this.thirdRoomParamY = thirdRoomParamY;
	}

	public String getFourthRoomParamX() {
		return fourthRoomParamX;
	}

	public void setFourthRoomParamX(String fourthRoomParamX) {
		this.fourthRoomParamX = fourthRoomParamX;
	}

	public String getFourthRoomParamY() {
		return fourthRoomParamY;
	}

	public void setFourthRoomParamY(String fourthRoomParamY) {
		this.fourthRoomParamY = fourthRoomParamY;
	}

}
