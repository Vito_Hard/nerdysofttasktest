package com.nerdysoft.test.entity;

import java.util.List;

public class RequestWrapperJson {
	private List<Room> room;

	public RequestWrapperJson(List<Room> room) {
		this.room = room;
	}

	public RequestWrapperJson() {
	}

	public List<Room> getRoom() {
		return room;
	}

	public void setRoom(List<Room> room) {
		this.room = room;
	}

}
