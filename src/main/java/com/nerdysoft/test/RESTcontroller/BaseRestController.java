package com.nerdysoft.test.RESTcontroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nerdysoft.test.entity.LegalRooms;
import com.nerdysoft.test.entity.RequestWrapperJson;
import com.nerdysoft.test.entity.Response;
import com.nerdysoft.test.entity.Room;
import com.nerdysoft.test.service.LegalRoomsService;

@RestController
@CrossOrigin(origins = "*")
public class BaseRestController {

	private LegalRoomsService legalRoomsService;

	@Autowired
	public BaseRestController(LegalRoomsService legalRoomsService) {
		this.legalRoomsService = legalRoomsService;
	}

	@PostMapping("/validateRoom")
	public ResponseEntity<?> saveArticles(@RequestBody String roomJson)
			throws JsonMappingException, JsonProcessingException {

		ObjectMapper objectMapper = new ObjectMapper();
		RequestWrapperJson c = objectMapper.readValue(roomJson, RequestWrapperJson.class);

		LegalRooms legalRooms = new LegalRooms();
		int i = 0;
		for (Room p : c.getRoom()) {
			i++;

			if (!(Integer.parseInt(p.getX()) >= 0 && Integer.parseInt(p.getX()) <= 35 && Integer.parseInt(p.getY()) >= 0
					&& Integer.parseInt(p.getY()) <= 35)) {
				return new ResponseEntity<Response>(new Response("param => 0 <= 35"), HttpStatus.BAD_REQUEST);
			}

			if (i == 1) {
				legalRooms.setFirstRoomParamX(p.getX());
				legalRooms.setFirstRoomParamY(p.getY());
			}

			if (i == 2) {
				legalRooms.setSecondRoomParamX(p.getX());
				legalRooms.setSecondRoomParamY(p.getY());
			}

			if (i == 3) {
				legalRooms.setThirdRoomParamX(p.getX());
				legalRooms.setThirdRoomParamY(p.getY());
			}

			if (i == 4) {
				legalRooms.setFourthRoomParamX(p.getX());
				legalRooms.setFourthRoomParamY(p.getY());
			}

		}

		legalRoomsService.saveLegalRooms(legalRooms);

		return new ResponseEntity<>(roomJson, HttpStatus.CREATED);
	}

}
